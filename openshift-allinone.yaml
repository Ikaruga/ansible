---
- name: OpenShift Origin All-in-one host install
  hosts: osnode1
  remote_user: python
  become: true

  vars:
    user: python
    openshift_package: "https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz"

  tasks:

    - name: install prereq packages
      yum:
        name:
          - centos-release-openshift-origin311
          - epel-release
          - docker
          - git
          - pyOpenSSL
        state: latest

    - name: make docker non-root execution
      user:
        name: "{{ user }}"
#        groups: dockerroot
        groups: root

    - name: start docker service
      service:
        name: docker
        state: started
        enabled: true

    - name: install openshift installer
      yum:
        name: openshift-ansible
        state: latest

    - name: configure insecure-registries
      lineinfile:
        path: /etc/docker/daemon.json
        state: present
        line:  '{ "insecure-registries": [ "172.30.0.0/16" ] }'
        regexp: '^{}'

    - name: reload and restart docker
      systemd:
        daemon_reload: yes
        name: docker
        state: restarted

    - name: add new zone to firewalld
      shell: "firewall-cmd --permanent --new-zone dockerc && firewall-cmd --reload"
      ignore_errors: yes

    - name: grant access
      firewalld:
        zone: dockerc
        immediate: true
        permanent: true
        source: 172.17.0.0/16
        state: enabled

    - name: grant access (8443)
      firewalld:
        zone: dockerc
        immediate: true
        permanent: true
        state: enabled
        port: 8443/tcp

    - name: grant access (53)
      firewalld:
        zone: dockerc
        immediate: true
        permanent: true
        state: enabled
        port: 53/udp

    - name: grant access (80, 8443, 443)
      firewalld:
        immediate: true
        permanent: true
        state: enabled
        port: "{{ item }}"
      with_items:
        - 8443/tcp
        - 443/tcp
        - 80/tcp

    - name: reload firewalld
      shell: "firewall-cmd --reload"
      ignore_errors: yes

    - name: retrieve latest openshift package
      get_url:
        url: "{{ openshift_package }}"
        dest: ~/
      become: False

    - name: get the package filename
      shell: "ls openshift*.tar.gz"
      register: package_name

    - name: unarchive openshift package
      unarchive:
        src: "~/{{ package_name.stdout }}"
        remote_src: True
        dest: '~/'
      become: False

    - name: delete openshift package
      file:
        path: "~/{{ package_name.stdout }}"
        state: absent
      become: False

    - name: get the package filename
      shell: "ls -d openshift*"
      register: installed_dir

    - name: set openshift path in .bash_profile
      lineinfile:
        line: "export PATH=$PATH:~/{{ installed_dir.stdout }}"
        path: "/home/{{ user }}/.bash_profile"

